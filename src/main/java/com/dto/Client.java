package com.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@NamedQueries({
	@NamedQuery(
	name = "findClient",
	query = "from Client c where c.areaOfExperties = :areaOfExperties"
	)
})

@Entity
@Table(schema = "test_hibernate", name="client", uniqueConstraints = {
		@UniqueConstraint(columnNames={"CLIENT_NAME", "AREA_OF_EXPERTIES"})})
		//@UniqueConstraint(columnNames = "AREA_OF_EXPERTIES"),
		//@UniqueConstraint(columnNames = "CLIENT_NAME"),
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CLIENT_ID")
	private Integer clientId;
	
	@Column(name = "CLIENT_NAME", nullable = false, length = 30)
	private String clientName;
	
	@Column(name = "AREA_OF_EXPERTIES", nullable = false, length = 30)
	private String areaOfExperties;

	@OneToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private Employee employee;
	
	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getAreaOfExperties() {
		return areaOfExperties;
	}

	public void setAreaOfExperties(String areaOfExperties) {
		this.areaOfExperties = areaOfExperties;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
}
