package com.service.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateObjectProviderImpl
{
	final SessionFactory sessionFactory =  new Configuration().configure().buildSessionFactory();
	
	public Session getHibernateSession()
	{
		Session session = sessionFactory.openSession();
		return session;
	}
}
