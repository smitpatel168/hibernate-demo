package com.service.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.dto.Client;
import com.dto.Employee;
import com.dto.Skill;
import com.dto.Team;
import com.service.HibernateQueryProvider;

@SuppressWarnings({ "unchecked" })
public class HibernateQueryProviderImpl implements HibernateQueryProvider {
	HibernateObjectProviderImpl hibernateObjectProvider = new HibernateObjectProviderImpl();

	public void selectEmployeeUsingSession() {
		Session session = hibernateObjectProvider.getHibernateSession();
		try {
			Employee employee = session.get(Employee.class, 1);
			System.out.println("First Name : " + employee.getEmployeeFirstName());
			
			// first level cache
			employee.setEmployeeFirstName("Smitt");
			Employee employee2 = session.get(Employee.class, 1);
			System.out.println("First Name : " + employee2.getEmployeeFirstName());
			
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public void selectEmployeeUsingHQL() {
		Session session = hibernateObjectProvider.getHibernateSession();
		try {
			Query query = session.createQuery("from Employee where employeeLastName = :employeeLastName ");
			query.setParameter("employeeLastName", "Patel");
			List<Employee> employeeList = query.list();
			for (Employee e : employeeList) {
				System.out.println("First Name : " + e.getEmployeeFirstName());
				System.out.println("Last Name : " + e.getEmployeeLastName());
				System.out.println("Salary : " + e.getEmployeeSalary());
				System.out.println("= = = =");
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void selectAllClientUsingNamedQuery() {
		Session session = hibernateObjectProvider.getHibernateSession();
		try {
			Query query = session.getNamedQuery("findClient").setString("areaOfExperties", "Defence");
			List<Client> list = query.list();
			for (Client client : list) {
				System.out.println("Client Id: " + client.getClientId());
				System.out.println("Client Name: " + client.getClientName());
				System.out.println("Experties: " + client.getAreaOfExperties());
				System.out.println("= = = =");
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void selectEmployeeUsingCriteria() {
		Session session = new Configuration().configure().buildSessionFactory().openSession();
		try {
			Criteria criteria2 = session.createCriteria(Employee.class)
					.addOrder(Order.asc("employeeFirstName"))
					.add(Restrictions.ge("employeeSalary", 10000));

			List<Employee> employeeList = criteria2.list();
			for (Employee e : employeeList) {
				System.out.println("First Name : " + e.getEmployeeFirstName());
				System.out.println("Second Name : " + e.getEmployeeLastName());
				System.out.println("Salary : " + e.getEmployeeSalary());
				System.out.println("= = = =");
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	public void selectEmployeeUsingNativeSQL() {
		Session session = new Configuration().configure().buildSessionFactory().openSession();
		try {
			Query query = session.createSQLQuery("select * from EMPLOYEE where EMPLOYEE_LAST_NAME =:employeeLastName")
					.addEntity(Employee.class).setParameter("employeeLastName", "Patel");
			List<Employee> employeeList = query.list();

			for (Employee e : employeeList) {
				System.out.println("First Name : " + e.getEmployeeFirstName());
				System.out.println("Last Name : " + e.getEmployeeLastName());
				System.out.println("Salary : " + e.getEmployeeSalary());
				System.out.println("= = = =");
			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public void selectTeamLazyLoading() {
		Session session = new Configuration().configure().buildSessionFactory().openSession();
		try {
			Query query = session.createQuery("from Team where teamName = :teamName")
					.setParameter("teamName", "WID");
			List<Team> teamList = query.list();

			for (Team t : teamList) {
				System.out.println("Team Name : " + t.getTeamName());

				// Lazy Loading - following code will trigger hibernate call to get employee list
				/*for(Employee e: t.getEmployee()){
					System.out.println("First Name : " + e.employeeFirstName);
					System.out.println("Last Name : " + e.getEmployeeLastName());
					System.out.println("Salary : " + e.getEmployeeSalary());
				}*/

			}
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public void cascadeTypeRemove() {
		Session session = new Configuration().configure().buildSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.delete(session.get(Employee.class, 2));
			
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void insertData() {
		Session session = hibernateObjectProvider.getHibernateSession();

		Client client1 = new Client();
		client1.setClientName("Saab");
		client1.setAreaOfExperties("Defence");
		
		Client client2 = new Client();
		client2.setClientName("Nasa");
		client2.setAreaOfExperties("Space");
		
		Client client3 = new Client();
		client3.setClientName("Lockheed Martin");
		client3.setAreaOfExperties("Defence");

		Team team1 = new Team();
		team1.setTeamName("i18n");
		
		Team team2 = new Team();
		team2.setTeamName("PLG");
		
		Team team3 = new Team();
		team3.setTeamName("WID");
		
		Employee employee1 = new Employee();
		employee1.setEmployeeFirstName("Smit");
		employee1.setEmployeeLastName("Patel");
		employee1.setEmployeeSalary(15000);
		employee1.setTeam(team1);
		employee1.setClient(client1);

		Employee employee2 = new Employee();
		employee2.setEmployeeFirstName("Umang");
		employee2.setEmployeeLastName("Patel");
		employee2.setEmployeeSalary(20000);
		employee2.setTeam(team1);
		employee2.setClient(client1);
		
		Employee employee3 = new Employee();
		employee3.setEmployeeFirstName("Ripal");
		employee3.setEmployeeLastName("Patel");
		employee3.setEmployeeSalary(25000);
		employee3.setTeam(team1);
		employee3.setClient(client1);
		
		Skill skill1 = new Skill();
		skill1.setSkillName("Java 1.6");
		skill1.setKnowledgeLevel("High");
		skill1.setEmployee(employee1);

		Skill skill2 = new Skill();
		skill2.setSkillName("J2EE");
		skill2.setKnowledgeLevel("Medium");
		skill2.setEmployee(employee1);
		
		Skill skill3 = new Skill();
		skill3.setSkillName("Spring");
		skill3.setKnowledgeLevel("Medium");
		skill3.setEmployee(employee1);
		
		Skill skill4 = new Skill();
		skill4.setSkillName("Java 1.7");
		skill4.setKnowledgeLevel("High");
		skill4.setEmployee(employee2);
		
		Skill skill5 = new Skill();
		skill5.setSkillName("JavaFX");
		skill5.setKnowledgeLevel("Medium");
		skill5.setEmployee(employee2);
		
		Skill skill6 = new Skill();
		skill6.setSkillName("Spring Boot");
		skill6.setKnowledgeLevel("Medium");
		skill6.setEmployee(employee2);
		
		Skill skill7 = new Skill();
		skill7.setSkillName("Java 1.8");
		skill7.setKnowledgeLevel("High");
		skill7.setEmployee(employee3);
		
		Skill skill8 = new Skill();
		skill8.setSkillName("Javascript");
		skill8.setKnowledgeLevel("Medium");
		skill8.setEmployee(employee3);
		
		Skill skill9 = new Skill();
		skill9.setSkillName("HTML5");
		skill9.setKnowledgeLevel("Medium");
		skill9.setEmployee(employee3);
		
		Employee employee4 = new Employee();
		employee4.setEmployeeFirstName("Pravin");
		employee4.setEmployeeLastName("Prajapati");
		employee4.setEmployeeSalary(30000);
		employee4.setTeam(team1);
		employee4.setClient(client1);
		
		Employee employee5 = new Employee();
		employee5.setEmployeeFirstName("Sagar");
		employee5.setEmployeeLastName("Gohil");
		employee5.setEmployeeSalary(15000);
		employee5.setTeam(team2);
		employee5.setClient(client2);

		Employee employee6 = new Employee();
		employee6.setEmployeeFirstName("Ketul");
		employee6.setEmployeeLastName("Patel");
		employee6.setEmployeeSalary(20000);
		employee6.setTeam(team2);
		employee6.setClient(client2);
		
		Employee employee7 = new Employee();
		employee7.setEmployeeFirstName("Udit");
		employee7.setEmployeeLastName("Mishra");
		employee7.setEmployeeSalary(25000);
		employee7.setTeam(team2);
		employee7.setClient(client2);
		
		Employee employee8 = new Employee();
		employee8.setEmployeeFirstName("Neha");
		employee8.setEmployeeLastName("Dhandhukiya");
		employee8.setEmployeeSalary(30000);
		employee8.setTeam(team2);
		employee8.setClient(client2);
		
		Employee employee9 = new Employee();
		employee9.setEmployeeFirstName("Yogesh");
		employee9.setEmployeeLastName("Patil");
		employee9.setEmployeeSalary(35000);
		employee9.setTeam(team2);
		employee9.setClient(client2);

		
		Employee employee10 = new Employee();
		employee10.setEmployeeFirstName("Ashish");
		employee10.setEmployeeLastName("Sangani");
		employee10.setEmployeeSalary(15000);
		employee10.setTeam(team3);
		employee10.setClient(client3);

		Employee employee11 = new Employee();
		employee11.setEmployeeFirstName("Nishith");
		employee11.setEmployeeLastName("Patel");
		employee11.setEmployeeSalary(20000);
		employee11.setTeam(team3);
		employee11.setClient(client3);
		
		Employee employee12 = new Employee();
		employee12.setEmployeeFirstName("Divyang");
		employee12.setEmployeeLastName("Golaviya");
		employee12.setEmployeeSalary(25000);
		employee12.setTeam(team3);
		employee12.setClient(client3);

		try {
			session.beginTransaction();

			session.save(team1);
			session.save(team2);
			session.save(team3);
			session.save(client1);
			session.save(client2);
			session.save(client3);
			
			session.save(skill1);
			session.save(skill2);
			session.save(skill3);
			session.save(skill4);
			session.save(skill5);
			session.save(skill6);
			session.save(skill7);
			session.save(skill8);
			session.save(skill9);
			
			session.save(employee1);
			session.save(employee2);
			session.save(employee3);
			session.save(employee4);
			session.save(employee5);
			session.save(employee6);
			session.save(employee7);
			session.save(employee8);
			session.save(employee9);
			session.save(employee10);
			session.save(employee11);
			session.save(employee12);
			
			session.getTransaction().commit();

		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
