package com.service.impl;

import com.service.HibernateQueryProvider;

public class HibernateDemoImpl {
	static HibernateObjectProviderImpl hibernateObjectProvider = new HibernateObjectProviderImpl();
	
	public static void main(String args[]) {
		HibernateQueryProvider queryExmple =  new HibernateQueryProviderImpl();
		
		// Insert records into databse
		//queryExmple.insertData();
		
		// Get Object using Session
		queryExmple.selectEmployeeUsingSession();

		// HQL Query
		//queryExmple.selectEmployeeUsingHQL();
		
		// Named Query
		//queryExmple.selectAllClientUsingNamedQuery();
		
		// Criteria Query
		//queryExmple.selectEmployeeUsingCriteria();
		
		// Native SQl Query
		//queryExmple.selectEmployeeUsingNativeSQL();

		// Lazy loading
		//queryExmple.selectTeamLazyLoading();
		
		// Cascade type Remove
		//queryExmple.cascadeTypeRemove();
	}
}
