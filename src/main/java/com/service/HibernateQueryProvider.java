package com.service;

public interface HibernateQueryProvider 
{
	public void insertData();
	
	public void selectEmployeeUsingSession();
	
	public void selectEmployeeUsingHQL();

	public void selectAllClientUsingNamedQuery();

	public void selectEmployeeUsingCriteria();

	public void selectEmployeeUsingNativeSQL();
	
	public void selectTeamLazyLoading();
	
	public void cascadeTypeRemove();
	
}
